package com.example.wesley.attractienl.stores;

import android.os.AsyncTask;

import com.example.wesley.attractienl.managers.HttpManager;
import com.example.wesley.attractienl.models.Attraction;
import com.example.wesley.attractienl.models.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class AttractionStore {

    public static ArrayList<Attraction> attractions;

    public static void getAttractions(){
        Request request = new Request();
        request.setMethod("GET");
        request.setUrl("https://mbodatastore.nl/attractie/pretparken.php");

        Async async = new Async();

        try{
            String resultString = async.execute(request).get();
            String parseString = resultString.replace("\n","");
            JSONArray jsonArray = new JSONArray(parseString);
            attractions = new ArrayList<Attraction>();
            for(int i=0;i<jsonArray.length();i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Attraction attraction = new Attraction();
                attraction.setId(jsonObject.getString("id"));
                attraction.setName(jsonObject.getString("naam"));
                attraction.setPlace(jsonObject.getString("plaats"));
                attraction.setLng(jsonObject.getString("lat"));
                attraction.setLat(jsonObject.getString("lng"));
                attraction.setImageName(jsonObject.getString("img"));
                attractions.add(attraction);
            }

        } catch (JSONException | ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Attraction getAttraction(int id){
        for(int i=0;i<attractions.size();i++){
            Attraction attraction = attractions.get(i);
            if(attraction.getId() == id){
                return attraction;
            }
        }
        return null;
    }

    private static class Async extends AsyncTask<Request, String, String> {
        @Override
        protected String doInBackground(Request... requests) {
            return HttpManager.getData(requests[0]);
        }
    }
}
