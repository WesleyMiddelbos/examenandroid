package com.example.wesley.attractienl.models;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class Request {
    private String url;
    private String method = "GET";
    private Map<String, String> params = new HashMap<>();

    public Request(){

    }

    public String getUrl(){
        return this.url;
    }

    public String getMethod(){
        return this.method;
    }

    public Map<String, String> getParams() {
        return this.params;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public void setMethod(String method){
        this.method = method;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void setParam(String key, String value) {
        this.params.put(key, value); //adds a single value to the params member variable
    }

    public String getEncodedParams() {
        StringBuilder sb = new StringBuilder();
        for (String key : params.keySet()) {
            String value = null;
            try {
                value = URLEncoder.encode(params.get(key), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(key + "=" + value);
        }
        return sb.toString();
    }
}
