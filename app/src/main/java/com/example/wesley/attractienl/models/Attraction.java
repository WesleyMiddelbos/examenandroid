package com.example.wesley.attractienl.models;

public class Attraction {

    private int id;
    private String name;
    private String place;
    private Double lat;
    private Double lng;
    private String imageName;

    public void setId(String id){
        this.id = Integer.parseInt(id);
    }

    public void setName(String name){
        this.name = name;
    }

    public void setPlace(String place){
        this.place = place;
    }

    public void setLat(String lat){
        this.lat = Double.parseDouble(lat);
    }

    public void setLng(String lng){
        this.lng = Double.parseDouble(lng);
    }

    public void setImageName(String imageName) {
        String name = imageName.replace(".jpg","");
        this.imageName = name;
    }

    public int getId(){
        return this.id;
    }

    public String getName(){
        return this.name;
    }

    public String getPlace(){
        return this.place;
    }

    public Double getLat(){
        return this.lat;
    }

    public Double getLng(){
        return this.lng;
    }

    public String getImageName(){
        return this.imageName;
    }
}
