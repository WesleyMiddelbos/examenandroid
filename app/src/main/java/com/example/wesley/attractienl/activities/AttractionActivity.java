package com.example.wesley.attractienl.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.wesley.attractienl.R;
import com.example.wesley.attractienl.models.Attraction;
import com.example.wesley.attractienl.stores.AttractionStore;

public class AttractionActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imageView;
    private TextView name;
    private TextView place;
    private Button showMap;
    private Attraction attraction;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attraction);

        this.imageView = findViewById(R.id.attractionActivityImage);
        this.name = findViewById(R.id.attractionActivityName);
        this.place = findViewById(R.id.attractionActivityPlace);
        this.showMap = findViewById(R.id.attractionActivityLocationButton);

        this.showMap.setOnClickListener(this);

        this.getAttraction();
        this.fillUi();
    }

    private void getAttraction(){
        int id = getIntent().getIntExtra("attraction",0);
        this.attraction = AttractionStore.getAttraction(id);
    }

    private void fillUi(){
        Resources resources = getResources();
        final int resourceId = resources.getIdentifier(attraction.getImageName(), "drawable",this.getPackageName());
        this.imageView.setImageDrawable(resources.getDrawable(resourceId));
        this.name.setText(attraction.getName());
        this.place.setText(attraction.getPlace());
        this.showMap.setTag(R.string.lngResource,attraction.getLng());
        this.showMap.setTag(R.string.latResource,attraction.getLat());
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this,MapsActivity.class);
        intent.putExtra("attraction",this.attraction.getId());
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.page_back:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
