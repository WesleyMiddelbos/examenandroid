package com.example.wesley.attractienl.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wesley.attractienl.R;
import com.example.wesley.attractienl.models.Attraction;
import com.example.wesley.attractienl.stores.AttractionStore;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private LinearLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AttractionStore.getAttractions();
        this.container = findViewById(R.id.mainScrollContainer);
        this.generateUi();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    private void generateUi() {
        ArrayList<Attraction> attractions = AttractionStore.attractions;
        Resources resources = this.getResources();
        for (int i = 0; i < attractions.size(); i++) {
            View child = getLayoutInflater().inflate(R.layout.attraction_scroll_layout, null);
            Attraction attraction = attractions.get(i);
            ImageView imageView = child.findViewById(R.id.attractionScrollLayoutImage);
            final int resourceId = resources.getIdentifier(attraction.getImageName(), "drawable", this.getPackageName());
            imageView.setImageDrawable(resources.getDrawable(resourceId));
            TextView attractionName = child.findViewById(R.id.attractionScrollLayoutName);
            attractionName.setText(attraction.getName());
            TextView attractionPlace = child.findViewById(R.id.attractionScrollLayoutPlace);
            attractionPlace.setText(attraction.getPlace());
            Button attractionButton = child.findViewById(R.id.attractionScrollLayoutButton);
            attractionButton.setTag(R.string.attractionResource, attraction);
            attractionButton.setOnClickListener(this);
            this.container.addView(child);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.attractionScrollLayoutButton:
                Intent intent = new Intent(MainActivity.this, AttractionActivity.class);
                Attraction attraction = (Attraction) view.getTag(R.string.attractionResource);
                intent.putExtra("attraction", attraction.getId());
                startActivity(intent);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Toast.makeText(this, "Nog geen locatie kunnen ophalen.", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
